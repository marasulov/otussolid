﻿using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using GuessNumber.Config;
using GuessNumber.IO;

namespace GuessNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfig<string> param1 = new JsonConfigReader(@"Config\conf.json");
            IConfig<string> param2 = new XmlConfigReader(@"D:\docs\Downloads\GuessNumber-master\GuessNumber-master\GuessNumber\Config\conf.xml");

            var tries = param1.GetParameter("Tries");
            var maxValue = param2.GetParameter("MaxValue");
            

            var dataConsole = new DataConsole();
            var dataWriter = new ConsoleWriter();
            
            var random = new Randomizer();
            var guess = new GuessNumber(random,dataConsole, dataConsole, 0,int.Parse(maxValue), int.Parse(tries));
            
            IResult result =  guess.Start();
            dataWriter.Write(result.GetResult());
        }
    }
}
