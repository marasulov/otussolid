﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber
{
    public class ResultSuccess : Result
    {
    

        public ResultSuccess():base()
        {
        }

        public override string GetResult()
        {
            return $"Вы выиграли!";
        }
    }
}
