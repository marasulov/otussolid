﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber
{
    public class ResultFail : Result
    {
        public ResultFail():base()
        {
        }

        public override string GetResult()
        {
            return $"Вы проиграли!";
        }
    }
}
