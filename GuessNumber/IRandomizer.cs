﻿namespace GuessNumber
{
    public interface IRandomizer<T>
    {
        T GetRandomNumber(T min, T max);
    }
}