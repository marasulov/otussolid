﻿using System.IO;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace GuessNumber.Config
{
    class JsonConfigReader:IConfig<string>
    {
        public string Filename { get; }

        public JsonConfigReader(string filename)
        {
            Filename = filename;
        }

        public string GetParameter(string parameterName)
        {
            var builder = new ConfigurationBuilder().AddJsonFile(Filename, false).Build();
            var settings = builder[parameterName];

            return settings;
        }
    }
}
