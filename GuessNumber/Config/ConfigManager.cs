﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.Config
{
    public class ConfigManager:IConfig<string>
    {
        private readonly IConfig<string> _config;

        public ConfigManager(IConfig<string> config)
        {
            _config = config;
        }

        public string Filename
        {
            get =>_config.Filename;
        }
        public string GetParameter(string parameterName)
        {
            return _config.GetParameter(parameterName);
        }
    }
}
