﻿using System.Xml;

namespace GuessNumber.Config
{
    class XmlConfigReader:IConfig<string>
    {
        public string Filename { get; }

        public XmlConfigReader(string filename)
        {
            Filename = filename;
        }

        public string GetParameter(string parameterName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(Filename);
            
            var element = (XmlElement)doc.SelectNodes("/Config/"+parameterName)[0];

            return element.InnerText;
        }
    }
}
