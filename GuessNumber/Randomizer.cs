﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber
{
    public class Randomizer:IRandomizer<int>
    {
        
        public int GetRandomNumber(int min, int max)
        {
            Random random = new();
            return random.Next(0, max);
        }
    }
}
