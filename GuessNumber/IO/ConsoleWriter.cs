﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessNumber.IO
{
    public class ConsoleWriter:IDataWriter
    {
        public void Write(string message)
        {
            Console.WriteLine(message);
        }
    }
}
