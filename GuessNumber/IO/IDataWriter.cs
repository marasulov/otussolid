﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace GuessNumber.IO
{
    public interface IDataWriter
    {
        void Write(string message);
    }
}
