﻿namespace GuessNumber
{
    public interface IResult
    {
        string GetResult();
    }
}