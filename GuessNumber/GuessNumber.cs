﻿using System;
using GuessNumber.IO;

namespace GuessNumber
{
    public class GuessNumber
    {
        private readonly int _maxTries;
        private readonly IRandomizer<int> _random;
        private readonly int _minNumber;
        private readonly int _maxNumber;
        private readonly IDataReader _reader;
        private readonly IDataWriter _writer;

        public GuessNumber(IRandomizer<int> random, IDataReader reader, IDataWriter writer, int minNumber = 0, int maxNumber = 10, int maxTries = 3)
        {
            _random = random;
            _minNumber = minNumber;
            _maxNumber = maxNumber;
            _maxTries = maxTries;
            _reader = reader;
            _writer = writer;
        }

        public IResult Start()
        {
            var guessedNumber = _random.GetRandomNumber(_minNumber, _maxNumber);
            int lastGuess = -1;
            int tries = 0;
            IResult result = new Result();
            while (lastGuess != guessedNumber && tries < _maxTries-1)
            {
                _writer.Write($"Введите число от {_minNumber} до {_maxNumber}");
                while (Int32.TryParse(_reader.Read(), out lastGuess))
                {
                    tries++;
                    _writer.Write($"осталось {_maxTries - tries} попыток");
                    if (lastGuess == guessedNumber)
                    {
                        result = new ResultSuccess();
                        break;
                    }

                    if (tries == _maxTries)
                    {
                        result = new ResultFail();
                        break;
                    }
                    
                    var message = lastGuess < guessedNumber ? "меньше" : "больше";
                    _writer.Write($"Введенное число {message}, чем загаданое");
                }
            }

            return result;
        }
    }
}
